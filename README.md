# ioquake3

![](medias/1637395437-screenshot.png)

ioquake3 for Linux:  raspberry pi, amd64 x86_64,... 


## Preinstalled Images with ioquake 3

### Debian Bullseye

Several images : https://gitlab.com/openbsd98324/ioquake3/-/tree/main/releases/debian-images


### OpenSuse Leap AMD64

https://gitlab.com/openbsd98324/ioquake3/-/tree/main/releases/v1-opensuse-leap


### Raspberry PI rpi3b


````
                mkdir -p /usr/lib/ioquake3/ 
                mkdir -p /opt/retropie/ports/quake3/  
             cd ; cd /opt/retropie/ports/  ; nconfig --fetch  'https://gitlab.com/openbsd98324/ioquake3.arm/-/raw/main/releases/v2/ioquake3.arm-retropie-retropie-4.5.1-rpi2_rpi3-default-v2.tar.gz'  ; tar xvpfz ioquake3.arm-retropie-retropie-4.5.1-rpi2_rpi3-default-v2.tar.gz   
            echo then run with  /opt/retropie/ports/quake3/ioquake3.arm 

````

### Raspberry Pi rpi4b 

````
cd ;  cd /usr/lib/ioquake3 ; ./ioquake3 +set cl_renderer opengl1 
````


